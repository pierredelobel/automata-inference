
#include <iostream>
#include <fstream>
#include <random>
#include <string>

#include <base/HillClimber.h>
#include <base/SimulatedAnnealingSearch.h>


int main(int argc, char ** argv) {

    int nbRun = 1;
    //int nbRun = atoi(argv[6]);
    int graine = 0;
    int nbStates = atoi(argv[3]);
    std::string graineFile = argv[4];
    std::string step = argv[5];
    nbRun = atoi(argv[1]);
    graine = atoi(argv[2]);

    std::cout << nbStates << " " << graineFile << " " << step << " " << nbRun << " ";

    HillClimber climber;
    SimulatedAnnealingSearch recuit;

    
    
    climber.run(nbRun, graine, nbStates, graineFile, step);
    recuit.run(nbRun, graine, nbStates, graineFile, step);

    return 0;
}