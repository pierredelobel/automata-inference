#ifndef _SIMULATED_H
#define _SIMULATED_H

#include <eval/smartEval.h>
#include "base/solution.h"

class SimulatedAnnealingSearch {
    public:
    SimulatedAnnealingSearch()= default;
    Solution<double> _best;
    std::vector<std::string> explode(std::string const & s, char delim)
    {
        std::vector<std::string> result;
        std::istringstream iss(s);

        for (std::string token; std::getline(iss, token, delim); )
        {
            result.push_back(std::move(token));
        }

        return result;
    }
    void run(int nbRun, int graine, int nbStates, std::string graineFile, std::string step) {

        std::string file = "../instances/dfa_" + std::to_string(nbStates) + "_"+ (graineFile) +"_"+ (step) +"_train-sample.json";
        Sample sample(file.c_str());

        // Création de la solution de base
        Solution<double> solution(nbStates,2);

        std::mt19937 gen( graine );
        for (int l = 0; l < solution.function.size(); ++l) {
            for (int i = 0; i < solution.function[i].size(); ++i) {
                solution.function[l][i] = gen() % (nbStates - 1);
            }
        }

        SmartEval seval(gen, sample);
        seval(solution);
        _best = solution;

        double u;
        int voisin = -1;
        double temperature = 0.03;
        double delta, alpha = 0.98;
        int palier=100, niv_palier=0, critArret = 0, i=0, j, k;
        double bestFitness = solution.fitness();
        int run = 0;
        bool stop = false;
        int previousVoisin[3] = {-1, -1, -1};
        int previous = -1;


        while  (i < nbRun && critArret < 3) {
            voisin =  rand() % solution.function.size();
            j = gen() % (nbStates);
            k = gen() % (1);
            int l = gen() % (nbStates - 1);
            previous = solution.function[j][k];
            solution.function[j][k] = l;
            seval(solution);
            delta = solution.fitness() - _best.fitness();
            if ( delta > 0) {
                _best = solution;
                niv_palier = 0;
                critArret = 0;
            } else {
                u = (rand() % 10000) / 10000.0;
                if (u < exp(delta/temperature)) {
                    _best = solution;
                    niv_palier = 0;
                    critArret = 0;
                }
                else {
                    solution.function[j][k] = previousVoisin[3];
                }
            }
            i++;
            if ((i % palier) == 0) {
                temperature = temperature * alpha;
                niv_palier++;
                critArret++;
            }
        }
        bestFitness = solution.fitness();

        file = "../instances/dfa_" + std::to_string(nbStates) + "_"+ (graineFile) +"_"+ (step) +"_test-sample.json";
        Sample sampleTest(file.c_str());
        SmartEval sevalTest(gen, sampleTest);
        sevalTest(solution);

        std::cout << bestFitness << " " << solution.fitness() << std::endl;
    }
};

#endif