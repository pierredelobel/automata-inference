#ifndef _HILLCLIMBERBEST_H
#define _HILLCLIMBERBEST_H

#include <string>
#include <vector>
#include <sstream>
#include <utility>

#include <eval/smartEval.h>
#include <array>
#include "base/solution.h"
#include "base/sample.h"

class HillClimber {
public :
    HillClimber()= default;
    Solution<double> _best;
    std::vector<std::string> explode(std::string const & s, char delim)
    {
        std::vector<std::string> result;
        std::istringstream iss(s);

        for (std::string token; std::getline(iss, token, delim); )
        {
            result.push_back(std::move(token));
        }

        return result;
    }
    void run(int nbRun, int graine, int nbStates, std::string graineFile, std::string step) {
        std::string file = "../instances/dfa_" + std::to_string(nbStates) + "_"+ (graineFile) +"_"+ (step) +"_train-sample.json";
        Sample sample(file.c_str());


        // Création de la solution de base
        Solution<double> solution(nbStates,2);

        std::mt19937 gen( graine );
        for (int l = 0; l < solution.function.size(); l++) {
            for (int i = 0; i < solution.function[i].size(); i++) {
                solution.function[l][i] = gen() % (nbStates - 1);
            }
        }
        SmartEval seval(gen, sample);
        seval(solution);
        _best = solution;


        double bestFitness = solution.fitness();
        double oldfit = 0.0;
        int nbIter = 0;
        bool stop = false;


        int voisin[3] = {-1, -1, -1};
        int previous = -1;


        while (!stop and nbIter < nbRun) {
            bestFitness = 0.0;
            oldfit = solution.fitness();
            voisin[0] = -1;
            voisin[1] = -1;
            voisin[2] = -1;
            for (int i = 0; i < nbStates; i++) {
                for (int j = 0; j < 2; j++) {
                    previous = solution.function[i][j];
                    // for (int k = 1 ; k < nbStates; k++) {
                        int k = gen() % (nbStates - 1);
                        // solution.function[i][j] = (previous + k) % nbStates;
                        solution.function[i][j] = k;
                        seval(solution);
                        // std::cout << "i => " << i << " j => " << j << " k => " <<  k << " => " << solution.fitness() << std::endl;
                        
                        if (solution.fitness() > bestFitness) {
                            bestFitness = solution.fitness();
                            voisin[0] = i;
                            voisin[1] = j;
                            voisin[2] = k;
                        }
                        solution.function[i][j] = previous;
                        nbIter++;
                    // }
                }

            }
            solution.fitness(oldfit);

            if(solution.fitness() < bestFitness and voisin[0] != -1 and voisin[1] != -1 and voisin[2] != -1)
            {
                solution.function[voisin[0]][voisin[1]] = voisin[2];
                solution.fitness(bestFitness);
            } else stop = true;

            
        }

        bestFitness = solution.fitness();

        file = "../instances/dfa_" + std::to_string(nbStates) + "_"+ (graineFile) +"_"+ (step) +"_test-sample.json";
        Sample sampleTest(file.c_str());
        SmartEval sevalTest(gen, sampleTest);
        sevalTest(solution);

        std::cout << " " << bestFitness << " " << solution.fitness() << " ";
    };

private:

};


#endif
