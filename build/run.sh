#!/bin/bash

nbRun=100

nbEvalList="100  1000 10000 100000"

nbStates=32
graine=5
step=0.1

echo random search

echo nbeval fitness > rs.csv

for n in ${nbEvalList}
do
  echo nbEval ${n}
  for((i = 0; i < ${nbRun};i++))
  do
    echo -n $i' '
    ./main $n $i $nbStates $graine $step >> rs_${nbStates}_${graine}_${step}.csv
  done
  echo
done
echo